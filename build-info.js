/* copy of server side json file created by alfa-build, editing here will not change values!  */
var buildInfo = {
    "gulpInfo": {
        "build": "22.4.12.21.5",
        "creator": "HP-Folio/admin7",
        "npmPackageVersion": "1.0.0",
        "appMainDir": "mfgsyscorp-website",
        "alfaBuilderVer": "22.1.20a"
    },
    "userInfo": {
        "Product info": {
            "Title": "mfgsyscorp.com",
            "Description": "",
            "Company": "Manufacturing Systems Corp."
        },
        "GATEWAY info": {
            "Version": ""
        },
        "Version info": {
            "Built By": "",
            "Version": "",
            "Date Created": ""
        },
        "Customer info": {
            "Customer": ""
        }
    },
    "gulpInfoProd": {
        "build": "22.4.12.21.6",
        "dateCreated": "Wed, 13 Apr 2022 01:06:28 GMT",
        "creator": "HP-Folio/admin7",
        "gulpFileLibVer": "21.5.14",
        "gulpArgs": null
    }
}
export default buildInfo